import React, {Component} from 'react';

class SearchPanel extends Component {

  state = {
    term: ''
  };

  onSearchChange = (e) => {
    const term = e.target.value;
    this.setState({term});
    this.props.onSearchChange(term);
  };

  render() {


    const searchText = 'Type here to search';
    return <input
      type="text"
      placeholder={searchText}
      className='form-control search-input'
      value={this.state.term}
      onChange={this.onSearchChange}/>
  }
}

export default SearchPanel;
