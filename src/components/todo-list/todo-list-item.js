import React, {Component} from 'react';

import './todo-list-item.css';

export default class TodoListItem extends Component {

  render() {

    const {label, onDeleted, onToggleImportant, onToggleDone, done, important} = this.props;

    let classNames = 'todo-list-item space-between d-flex';
    if (done) {
      classNames += ' done';
    } else {

    }

    if (important) {
      classNames += ' important';
    }

    return (
    <span className={classNames}>
      <span
        className="todo-list-item-label"
        onClick={onToggleDone}>
        {label}
      </span>
      <div>
        <button type='button' onClick={onDeleted} className='btn btn-outline-danger m-1'><i className='fa fa-trash-o'/></button>
        <button type='button' onClick={onToggleImportant} className="btn btn-outline-success"><i className="fa fa-exclamation"/></button>
      </div>
    </span>
    )
  }
}
